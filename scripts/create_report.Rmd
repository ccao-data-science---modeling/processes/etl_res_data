---
title: "Data Integrity Report"
date: "`r format(Sys.time(), '%d %B, %Y @ %r')`"
output: html_document
---

```{r, include=FALSE, message=FALSE}

# Load libraries specifically necessary for this report
library(arrow)
library(dplyr)
library(ggplot2)
library(here)
library(skimr)

# Load previously save modeldata and assmntdata files
mdata <- read_parquet(here("output", "modeldata.parquet"))
adata <- read_parquet(here("output", "assmntdata.parquet"))

# The goal of this script is to flag any data issues (especially missingness)
# which could affect modeling and assessment. As such, we start with 0 warnings
# and run a series of checks that += 1 to this number. Any number > 0 is bad
num_warnings <- 0
```

# Overall Data Issues

### Columns

Columns in `modeldata` not in `assmntdata`:

```{r, echo=FALSE}
cols_ma <- names(mdata)[!names(mdata) %in% names(adata)]
ifelse(length(cols_ma) > 0, cols_ma, "None")
expected_cols <- c(
  "meta_sale_price",
  "ind_arms_length",
  "meta_document_num",
  "meta_deed_type",
  "meta_num_288s_active"
)

num_warnings <- num_warnings + ifelse(!all(cols_ma %in% expected_cols), sum(!cols_ma %in% expected_cols), 0)
```

Columns in `assmntdata` not in `modeldata`:

```{r, echo=FALSE}
cols_am <- names(adata)[!names(adata) %in% names(mdata)]
ifelse(length(cols_am) > 0, cols_am, "None")
expected_cols <- c(
  "meta_num_288s_ended"
)

num_warnings <- num_warnings + ifelse(!all(cols_am %in% expected_cols), sum(!cols_am %in% expected_cols), 0)
```

### Factor Levels

Factors with non-matching levels:

```{r, echo=FALSE}

# Find factors with non-matching levels
fct_cols <- mdata[sapply(mdata, is.factor)]
fct_cols_lvls <- lapply(names(fct_cols), function(x) setdiff(
  levels(mdata[[x]]), 
  levels(adata[[x]]))
)
names(fct_cols_lvls) <- names(fct_cols)
fct_cols_lvls <- fct_cols_lvls[sapply(fct_cols_lvls, function(x) length(x) > 0)]
ifelse(length(fct_cols_lvls) > 0, fct_cols_lvls, "None")

num_warnings <- num_warnings + length(fct_cols_lvls)
```

Factor levels for each factor with differences:

```{r, echo=FALSE}
for (col in names(fct_cols_lvls)) {
  print(paste("Factor levels for col:", col))
  print(paste("Levels in modeldata:", paste(levels(mdata[[col]]), collapse = ", ")))
  print(paste("Levels in assmntdata:", paste(levels(adata[[col]]), collapse = ", ")))
}
```

### Missing Values

Number of critical missing values in assessment data:

```{r, echo=FALSE}
cols_check_missing <- c("geo_latitude", "geo_longitude", "meta_nbhd_avg")

adata_missing <- sapply(cols_check_missing, function(x) sum(is.na(adata[[x]])))

if (sum(adata_missing) > 0) {
  for (i in seq_len(length(cols_check_missing))) {
    print(paste(
      "Column:", cols_check_missing[i],
      "has", adata_missing[i], "missing values"
    ))
  }
}

num_warnings <- num_warnings + any(as.logical(sign(adata_missing)))
```

# Variable Distributions

### Sale Price

```{r, echo=FALSE, warning=FALSE, message=FALSE}
ggplot(mdata) +
  geom_histogram(aes(x = meta_sale_price), fill="blue", binwidth = 10000) +
  scale_x_continuous(labels = scales::dollar, limits = c(0, 1.5e6)) +
  labs(title = "Distribution of Sale Price (Model Data)", x = "Sale") +
  theme_minimal()

```

```{r, echo=FALSE, warning=FALSE, message=FALSE}
ggplot(mdata) +
  geom_histogram(aes(x = char_bldg_sf), fill="blue") +
  scale_x_continuous(labels = scales::comma, limits = c(0, 7500)) +
  labs(title = "Distribution of Bldg. Sqft. (Model Data)", x = "Bldg. Sqft.") +
  theme_minimal()

ggplot(adata) +
  geom_histogram(aes(x = char_bldg_sf), fill="blue") +
  scale_x_continuous(labels = scales::comma, limits = c(0, 7500)) +
  labs(title = "Distribution of Bldg. Sqft. (Assessment Data)", x = "Bldg. Sqft.") +
  theme_minimal()

```


# Model Data Summary

```{r, echo=FALSE, message=FALSE}
skim(mdata)
```

# Assessment Data Summary

```{r, echo=FALSE, message=FALSE}
skim(adata)
```

```{r num_warnings, results='asis', echo=FALSE}
format_num <- function(x) {
  color_a <- "#81ca9c"
  color_b <- "#CD5C5C"
  msg <- "Number of warnings: "
  formatted = noquote(ifelse(
    x == 0,
    paste0(sprintf("<h2><span style='background-color:%s'>", color_a), msg, x, "</span></h2>"),
    paste0(sprintf("<h2><span style='background-color:%s'>", color_b), msg, x, "</span></h2>")
  ))
  cat(formatted)
}

format_num(num_warnings)
```

